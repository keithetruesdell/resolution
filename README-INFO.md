READ the `HELPME.md` file for the original layout and design.  
The `README.md` file is symlinked to the current year.  This is done by design.
When viewing the repo online, the README file will be displayed.  By symlinking it to the most recent year, this will allow the repo to view it online, while still being able to 'edit' the symlink, and the year of the file.
Adjust the symlink yearly by just overwriting it.
`ln -svf 2021-resolution.md README.md`
That will override the symlink the file for the 2021 resolution.  

Personal things overriding the original HELPME file...

1) Year and the Resolution for the top  
2) Every resolution hopefully has progress bar  
3)
