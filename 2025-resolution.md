# 2025 Resolutions  

## Resoltuion 1 - ![progress](https://img.shields.io/badge/progress-0%25-red.svg)  

### Read the Bible Entirely and other religious texts  

- [ ] Read the GIRM
- [ ] Read the Missal

## Resolution 2 - ![](https://img.shields.io/badge/progress-0%25-red.svg)

### Read 5 books

- [ ] Knight one from Mark Dugay
- [ ] Screwtape letters
- [ ] Not Your Average Joe
- [ ]
- [ ]

## Resolution 3 💪🏼 - ![](https://img.shields.io/badge/progress-0%25-red.svg)

Lose 8+ lbs - get to 185 lbs

- **Jan 1 2024** - ~195 lbs

## Resolution 3 💪🏼 - ![](https://img.shields.io/badge/progress-0%25-red.svg)

Run 400 miles

Run 5k sub 25min

Bond Lake "records"

| Place | Total Time | Mile Time | Pace / MPH |
|:-----:|:-----------|:---------:|:----------:|
| 3     | 30:45      | 9:45      | 6.15       |
| even  | 28:23      | 9:00      | 6.65       |
| 2     | 26:01      | 8:15      | 7.27       |
| even  | 24:51      | 8:00      | 7.50       |
| 1     | 23:39      | 7:30      | 8.00       |

Currently running about 28:30 - 32:30 at Bond Lake  
Approx pace
9:21 (max) - 10:26

- **Jan 1 2024** -  

## Resolution ‍4 💻 - ![progress](https://img.shields.io/badge/progress-0%25-red.svg)

### Work on 5 open source projects

- [ ]  
- [ ]  
- [ ]  
- [ ]  
- [ ]  

## Resolution 5 🤝 ![](https://img.shields.io/badge/progress-0%25-red.svg)

### Publish at least 12 articles

Use the KeithETruesdell, Keysh, or ChurchITGuy blogs.  
Look into syncing these with 'dev.to' and 'medium'.

- [ ]  

## Resolution 6 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  

### Make wood carving for Louise

## Resolution 7 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  

### Start the 'Dozen Dads' blog & site

## Resolution 8 💻 - ![](https://img.shields.io/badge/progress-0%25-red.svg)

### Create Calculator for macOs

## Resolution 9 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  

### Finish Church Icon tracker

- API
- blog
- webpage
- app
- documentation

## Resolution 10 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  

### Create Webpage for Acutis Organization  

- [ ] Create proper 'Code of Conduct' for Acutis

## Resolution 11

### Remove 1 app a month

And write a review on it

## Resolution 12 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  

### Make Shaving Horses  

- [ ] for myself  
- [ ] Bella  
- [ ] Jake  
- [ ] Boys
