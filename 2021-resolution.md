## Resoltuion 1 - ![](https://img.shields.io/badge/progress-0%25-red.svg)
### Read the Bible Entirely

* **Jan 1 2020** - Genesis


## Resolution 2 - ![](https://img.shields.io/badge/progress-0%25-red.svg)
### Read 10 books

- [ ] Knight one from Mark Dugay
- [ ] Screwtape letters
- [ ] Not Your Average Joe
- [ ] How To
- [ ] 


## Resolution 3 💪🏼 - ![](https://img.shields.io/badge/progress-0%25-red.svg)
Lose 15 lbs - get to 198 lbs

* **Jan 1 2020** - 215 lbs


## Resolution ‍4 💻 - ![](https://img.shields.io/badge/progress-0%25-red.svg)
### Work on 5 open source projects

- [ ]  
- [ ]  
- [ ]  
- [ ]  
- [ ]  


## Resolution 5 🤝 ![](https://img.shields.io/badge/progress-0%25-red.svg)
### Publish at least 20 articles.
Use the KeithETruesdell, Keysh, or ChurchITGuy blogs.  
Look into syncing these with 'dev.to' and 'medium'.

- [ ]  


## Resolution 6 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
### Make wood carving for Louise 


## Resolution 7 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
### Start the 'Dozen Dads' blog & site


## Resolution ‍8 💻 - ![](https://img.shields.io/badge/progress-0%25-red.svg)
#### Create Calculator for Atom.io  


## Resolution 9 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
#### Finish and opensource CAPI-DAPI 


## Resolution 10 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
#### Finish Church Icon tracker API 


## Resolution 11 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
### Create Webpage for Church Icon Tracker 


## Resolution 12 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
### Create Webpage for Church Icon Tracker API Documentation  


## Resolution 13 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
### Create Android App for Church Icon Tracker  


## Resolution 14 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
### Create Webpage for Acutis Organization  

- [ ] Create proper 'Code of Conduct' for Acutis 


## Resolution 15 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
### Make Shaving Horses  

- [ ] for myself  
- [ ] Bella  
- [ ] Jake  
- [ ] Boys  
 

## Resolution 16 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
### Create something for Liz 


## Resolution 17 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
### Side Stair planter


## Resolution 18 - ![](https://img.shields.io/badge/progress-0%25-red.svg)  
### Grill area

